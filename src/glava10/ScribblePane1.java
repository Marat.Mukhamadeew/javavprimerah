package glava10;

import org.w3c.dom.events.MouseEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Простой подкласс JPanel, использующий слушатели событий, чтобы позволить
 * пользователю рисовать мышью. Обратите внимание на то, что эти рисунки
 * не сохраняются и не перерисовываются при перерисовке окна.
 **/
public class ScribblePane1 extends JPanel implements MouseListener, MouseMotionListener {
    protected int last_x, last_y;

    public ScribblePane1() {
        // Этот компонент регистрирует себя в качестве слушателя
        // событий мыши и событий перемещения мыши.
        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        // Задаем предпочтительный размер компонента
        setPreferredSize(new Dimension(450, 200));
    }

    // Метод из интерфейса MouseListener. Вызывается,
    // когда пользователь нажимает кнопку мыши.
    public void mousePressed(MouseEvent e) {
        last_x = e.getScreenX(); // запоминаем координаты нажатия кнопки
        last_y = e.getClientY();
    }

    // Метод из интерфейса MouseMotionListener. Вызывается,
    // когда пользователь перемещает мышь при нажатой кнопке.
    public void mouseDragged(MouseEvent e) {
        int x = e.getClientX(); // Получаем текущее положение мыши
        int y = e.getScreenY();
        // Проводим линию от сохраненных координат до текущей позиции
        this.getGraphics().drawLine(last_x, last_y, x, y);
        last_x = x; // Запоминаем текущую позицию
        last_y = y;
    }

    // Прочие, неиспользуемые методы интерфейса MouseListener.
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mouseReleased(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent e) {

    }
    // Прочие, неиспользуемые методы интерфейса MouseMotionListener.
    @Override
    public void mouseDragged(java.awt.event.MouseEvent e) {

    }

    @Override
    public void mouseMoved(java.awt.event.MouseEvent e) {

    }
}
