package glava10;

import javax.swing.*;
import java.awt.*;

public class GridBagLayoutPane extends JPanel {
    public GridBagLayoutPane() {
        // Создаем и назначаем менеджер компоновки
        this.setLayout(new GridBagLayout());

        // Создаем объект ограничений и задаем некоторые принимаемые
        // по умолчанию значения
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5,5,5,5);

        // Создаем и помещаем в контейнер группу кнопок, задавая для каждой разные
        // размеры и положение в решетке. Полагаем для первой кнопки коэффициент
        // чувствительности к размеру окна равным 1.0, а для других равным 0.0.
        // Первая кнопка будет занимать все освободившееся пространство.
        c.gridx = 0; c.gridy = 0; c.gridwidth = 4; c.gridheight=4;
        c.weightx = c.weighty = 1.0;
        this.add(new JButton("Button #1"), c);
        c.gridx = 4; c.gridy = 0; c.gridwidth = 1; c.gridheight=1;
        c.weightx = c.weighty = 0.0;
        this.add(new JButton("Button #2"), c);
        c.gridx = 4; c.gridy = 1; c.gridwidth = 1; c.gridheight=1;
        this.add(new JButton("Button #3"), c);
        c.gridx = 4; c.gridy = 2; c.gridwidth = 1; c.gridheight=2;
        this.add(new JButton("Button #4"), c);
        c.gridx = 0; c.gridy = 4; c.gridwidth = 1; c.gridheight=1;
        this.add(new JButton("Button #5"), c);
        c.gridx = 2; c.gridy = 4; c.gridwidth = 1; c.gridheight=1;
        this.add(new JButton("Button #6"), c);
        c.gridx = 3; c.gridy = 4; c.gridwidth = 2; c.gridheight=1;
        this.add(new JButton("Button #7"), c);
        c.gridx = 1; c.gridy = 5; c.gridwidth = 1; c.gridheight=1;
        this.add(new JButton("Button #8"), c);
        c.gridx = 3; c.gridy = 5; c.gridwidth = 1; c.gridheight=1;
        this.add(new JButton("Button #9"), c);
    }
}
