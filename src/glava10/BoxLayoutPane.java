package glava10;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class BoxLayoutPane extends JPanel {
    public BoxLayoutPane() {
        // Применяем менеджер компоновки BorderLayout для размещения
        // разных компонентов Box
        this.setLayout(new BorderLayout());

        // Задаем поля для всей панели, помещая в нее пустую рамку
        // Это также можно было бы сделать, заместив getInsets()
        this.setBorder(new EmptyBorder(10,10,10,10));

        // Размещаем вдоль верхнего края панели простой ряд кнопок
        Box row = Box.createHorizontalBox();
        for (int i = 0; i < 4; i++) {
            JButton b = new JButton("B" + i);
            b.setFont(new Font("serif", Font.BOLD, 12 + i * 2));
            row.add(b);
        }
        this.add(row, BorderLayout.NORTH);

        // Размещаем простой столбец кнопок вдоль правого края панели
        // Применяем BoxLayout для другого Swing-контейнера
        // Создаем рамку вокруг столбца: это не получилось бы с классом Box
        JPanel col = new JPanel();
        col.setLayout(new BoxLayout(col, BoxLayout.Y_AXIS));
        col.setBorder(new TitledBorder(new EtchedBorder(),"Column"));

        for (int i = 0; i < 4; i++) {
            JButton b = new JButton("Button " + i);
            b.setFont(new Font("sansserif",Font.BOLD,10 + i * 2));
            col.add(b);
        }
        this.add(col, BorderLayout.EAST);

        // Размещаем прямоугольник с кнопками вдоль нижнего края панели.
        // Применяем «Glue» для равномерного распределения кнопок
        Box buttonBox = Box.createHorizontalBox();
        buttonBox.add(Box.createHorizontalGlue()); // эластичный промежуток
        buttonBox.add(new JButton("Okay"));
        buttonBox.add(Box.createHorizontalGlue()); // эластичный промежуток
        buttonBox.add(new JButton("Cancel"));
        buttonBox.add(Box.createHorizontalGlue()); // эластичный промежуток
        buttonBox.add(new JButton("Help"));
        buttonBox.add(Box.createHorizontalGlue()); // эластичный промежуток
        this.add(buttonBox, BorderLayout.SOUTH);

        // Создаем, чтобы отобразить в центре панели
        JTextArea textArea = new JTextArea();
        textArea.setText("This component has 12-pixel margins on left and top and has 72-pixel margins on right and bottom.");
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);

        // Используем объекты Box, чтобы окружить JTextArea нестандартными полями
        // Во-первых, создаем столбец из трех элементов. Первый и последний будут
        // жесткими промежутками. Средний элемент будет текстовой областью
        Box fixedCol = Box.createVerticalBox();
        fixedCol.add(Box.createVerticalStrut(12));
        fixedCol.add(textArea);
        fixedCol.add(Box.createVerticalStrut(72));

        // Теперь создаем строку. Помещаем распорки слева и справа,
        // а определенный выше столбец вставляем посередине.
        Box fixedRow = Box.createHorizontalBox();
        fixedRow.add(Box.createHorizontalStrut(12));
        fixedRow.add(fixedCol);
        fixedRow.add(Box.createHorizontalStrut(72));

        // Теперь помещаем в панель область JtextArea, заключенную в столбец
        // и в строку
        this.add(fixedRow, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        BoxLayoutPane blp = new BoxLayoutPane();
    }
}
