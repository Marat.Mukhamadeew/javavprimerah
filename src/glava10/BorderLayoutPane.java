package glava10;

import javax.swing.*;
import java.awt.*;

public class BorderLayoutPane extends JPanel {
    String[] borders = { "North", "East", "South", "West", "Center" };
    public BorderLayoutPane() {
        // Применяем BorderLayout с промежутками в 10 пикселов между компонентами
        this.setLayout(new BorderLayout(10,10));
        for (int i = 0; i < 5; i++) {
            this.add(new JButton(borders[i]), borders[i]);
        }
    }
}
