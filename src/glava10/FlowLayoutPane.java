package glava10;

import javax.swing.*;
import java.awt.*;

public class FlowLayoutPane extends JPanel {
    public FlowLayoutPane() {
        // Применяем менеджер компоновки FlowLayout. Строки выровнены по левому
        // краю. Между компонентами – и по горизонтали, и по вертикали 
        // оставляется по 10 пикселов.
        this.setLayout(new FlowLayout(FlowLayout.LEFT,10,10));

        // Добавляем несколько кнопок, чтобы продемонстрировать компоновку.
        StringBuilder spaces = new StringBuilder();
        for (int i = 0; i <= 9; i++) {
            this.add(new JButton("Button #" + i + spaces));
            spaces.append(" ");
        }
        // Сами задаем принимаемый по умолчанию размер
        this.setPreferredSize(new Dimension(500,200));
    }
}
