package glava2;

/**
 * Этот класс представляет прямоугольник. Его поля представляют координаты
 * углов этого прямоугольника. Его методы определяют операции, которые
 * могут осуществляться с объектами Rect.
 */

public class Rect {
    public int x1, y1, x2, y2;

    /**
     * Метод Rect - главный конструктор класса. Он просто использует свои
     * аргументы для инициализации полей нового объекта. Обратите внимание
     * на то, что его имя совпадает с именем класса и что в его сигнатуре
     * нет возвращаемого значения
     */
    public Rect(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    /**
     * Это еще один конструктор. Он определяется через предыдущий конструктор
     */
    public Rect(int width, int height) {
        this(0, 0, width, height);
    }

    /**
     * Это еще один конструктор
     */
    public Rect() {
        this(0, 0, 0, 0);
    }

    /**
     * Перемещение прямоугольника на указанные расстояния
     */
    public void move(int deltaX, int deltaY) {
        x1 += deltaX;
        x2 += deltaX;
        y1 += deltaY;
        y2 += deltaY;
    }

    /**
     * Проверяем, находится ли заданная точка внутри прямоугольника
     */
    public boolean isInside(int x, int y) {
        return ((x >= x1) && (x <= x2) && (y >= y1) && (y <= y2));
    }

    /**
     * Возвращается объединение этого прямоугольника с другим. Другими словами
     * возвращается наименьший прямоугольник, содержащий оба прямоугольника.
     */
    public Rect union(Rect r) {
        return new Rect(Math.min(this.x1, r.x1),
                Math.min(this.y1, r.y1),
                Math.max(this.x2, r.x2),
                Math.max(this.y2, r.y2));
    }

    /**
     * Возвращается пересечение этого прямоугольника с другим.
     * Другими словами, возвращается область их наложения
     * */
    public Rect intersection(Rect r) {
        Rect result = new Rect((this.x1 > r.x1) ? this.x1 : r.x1,
                (this.y1 > r.y1) ? this.y1 : r.y1,
                (this.x2 < r.x2) ? this.x2 : r.x2,
                (this.y2 < r.y2) ? this.y2 : r.y2);
        if (result.x1 > result.x2) result.x1 = result.x2 = 0;
        if (result.y1 > result.y2) result.y1 = result.y2 = 0;
        return result;
    }

    /**
     * Это метод нашего базового класса Object. Мы замещаем его так, чтобы
     * объекты класса Rect можно было осмысленно преобразовывать в строки,
     * складывать с другими строками при помощи оператора + и передавать
     * таким методам, как System.out.println().
     */

    @Override
    public String toString() {
        return "[" + x1 + ", " + y1 + ", " + x2 + ", " + y2 + "]";
    }
}
