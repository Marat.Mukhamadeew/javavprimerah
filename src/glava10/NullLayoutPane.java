package glava10;

import javax.swing.*;
import java.awt.*;

public class NullLayoutPane extends JPanel {
    public NullLayoutPane() {
        // Освобождаемся от принимаемого по умолчанию менеджера компоновки.
        // Будем сами размещать компоненты.
        this.setLayout(null);
        // Создаем несколько кнопок и устанавливаем их размеры и положения
        // явным образом
        for (int i = 1; i <= 9; i++) {
            JButton b = new JButton("Button #" + i);
            b.setBounds(i * 30, i * 20, 125, 30); // в Java 1.0 используйте reshape()
            this.add(b);
        }
    }

    // Указываем, какого размера должна быть панель.
    public Dimension getPreferredSize() {
        return new Dimension(425, 250);
    }
}
