package glava10;

import javax.swing.*;
import java.awt.*;

/**
 * Подкласс компонента, демонстрирующий вложенные контейнеры и компоненты.
 * Он создает приведенную ниже иерархию и использует различные цвета
 * для различения глубины вложенности контейнеров
 *
 * containers---panel1----button1
 *           |       |----panel2----button2
 *           |       |         |----panel3----button3
 *           |       |-------panel4----button4
 *           |                  |----button5
 *           |---button6
 */
public class Containers extends JPanel {
    public Containers() {
        this.setBackground(Color.white);
        this.setFont(new Font("Dialog", Font.BOLD,24));

        JPanel p1 = new JPanel();
        p1.setBackground(new Color(200, 200, 200)); // Panel1 темнее
        this.add(p1); // p1 содержится в этом компоненте
        p1.add(new JButton("#1")); // Button1 содержится в p1
        JPanel p2 = new JPanel();
        p2.setBackground(new Color(150, 150, 150)); // p2 темнее, чем p2
        p1.add(p2); // p2 содержится в p1
        p2.add(new JButton("#2")); // Button2 содержится в p2
        JPanel p3 = new JPanel();
        p3.setBackground(new Color(100, 100, 100)); // p3 темнее, чем p2
        p2.add(p3); // p3 содержится в p2
        p3.add(new JButton("#3")); // Button3 содержится в p3
        JPanel p4 = new JPanel();
        p4.setBackground(new Color(150, 150, 150)); // p4 темнее, чем p1
        p1.add(p4); // p4 содержится в p1
        p4.add(new JButton("#4")); // Button4 содержится в p4
        p4.add(new JButton("#5")); // Button5 тоже содержится в p4
        this.add(new JButton("#6")); // Button6 содержится в этом компоненте
    }
}
