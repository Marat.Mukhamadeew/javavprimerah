package glava10;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Vector;

/**
 * Этот класс является программой, которая использует отражение и JavaBeans
 * интроспекцию для создания набора заданных компонентов, установки заданных
 * свойств этих компонентов и их отображения на экране. Она позволяет
 * пользователю увидеть компоненты с применением любого установленного стиля.
 * Она создана как простое средство для экспериментирования с компонентами
 * AWT и Swing и для просмотра других примеров, предложенных в этой главе.
 * Она демонстрирует также окна (frames), меню и компонент JTabbedPane.
 */
public class ShowComponent {
    // Главная программа
    public static void main(String[] args) {
        // Обрабатываем командную строку для получения отображаемых компонентов
        Vector components = getComponentsFromArgs(args);

        // Создаем окно для отображения в нем компонентов
        JFrame frame = new JFrame("Show Component");

        // На закрытие окна реагируем выходом из VM

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // Устанавливаем меню, позволяющее пользователю выбрать
        // стиль компонента в списке установленных PLAF
        JMenuBar menubar = new JMenuBar();
        frame.setJMenuBar(menubar);
        JMenu plafmenu = createPlafMenu(frame);
        menubar.add(plafmenu);

        // Создаем JTabbedPane для отображения всех компонентов
        JTabbedPane pane = new JTabbedPane();

        // Теперь помещаем каждый компонент как вкладку на панель со вкладками
        // Используем сокращенное имя класса компонента как текст на ярлыке вкладки
        for (int i = 0; i < components.size(); i++) {
            Component c = (Component) components.elementAt(i);
            String className = c.getClass().getName();
            String tabName = className.substring(className.lastIndexOf('.' + 1));
            pane.addTab(tabName, c);
        }

        // Помещаем панель с вкладками в окно. Обратите внимание на вызов
        // getContentPane(). Это обязательно для JFrame, но не необходимо
        // для большинства компонентов Swing
        frame.getContentPane().add(pane);

        // Устанавливаем размер окна и показываем его
        frame.pack();
        frame.setVisible(true);
        // Здесь происходит выход из метода main(), но Java VM продолжает
        // работать, т. к. все AWTпрограммы автоматически запускают поток
        // (thread) обработки событий.
    }

    /*
     * Этот статический метод опрашивает систему, желая узнать, какие
     * подключаемые стили (Pluggable LookandFeel, PLAF) доступны.
     * Затем он создает компонент JMenu, перечисляющий все стили по имени
     * и позволяющий пользователю выбрать один из них при помощи компонентов
     * JRadioButtonMenuItem. Когда пользователь сделает выбор, выбранный
     * элемент меню обходит иерархию компонентов, приказывая каждому
     * компоненту применить новый PLAF.
     **/
    public static JMenu createPlafMenu(final JFrame frame) {
        // Создаем меню
        JMenu plafMenu = new JMenu("Look and Feel");

        // Создаем объект для взаимоисключающего выбора на основе переключателей (radio button)
        ButtonGroup radioGroup = new ButtonGroup();

        // Просматриваем доступные стили
        UIManager.LookAndFeelInfo[] plafs = UIManager.getInstalledLookAndFeels();

        // Проходим стили в цикле, помещая в меню элемент для каждого из них
        for (int i = 0; i < plafs.length; i++) {
            String plafName = plafs[i].getName();
            final String plafClassName = plafs[i].getClassName();

            // Создаем элемент меню
            JMenuItem item = plafMenu.add(new JRadioButtonMenuItem(plafName));

            // Сообщаем элементу меню, что нужно делать, если его выберут
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        // Устанавливаем новый стиль
                        UIManager.setLookAndFeel(plafClassName);
                        // Приказываем каждому компоненту изменить свой стиль
                        SwingUtilities.updateComponentTreeUI(frame);
                        // Приказываем окну изменить его размер в соответствии
                        // с новыми желаемыми размерами его элементов
                        frame.pack();
                    } catch (Exception ex) {
                        System.err.println(ex);
                    }
                }
            });
            // Только один элемент меню может быть выбран в каждый момент
            radioGroup.add(item);
        }
        return plafMenu;
    }

    /*
     * Этот метод в цикле проходит аргументы, заданные в командной строке,
     * отыскивая имена классов компонентов для создания компонентов и установки
     * их свойств, заданных в форме name=value. Этот метод демонстрирует
     * отражение и интроспекцию JavaBeans так, как они могут быть применены
     * к динамически создаваемому GUI
     **/
    public static Vector getComponentsFromArgs(String[] args) {
        Vector components = new Vector();
        Component component = null;
        PropertyDescriptor[] properties = null;
        Object[] methodArgs = new Object[1];

        nextarg:
        for (int i = 0; i < args.length; i++) {
            // Если аргумент не содержит знак равенства, значит, это
            // имя класса компонента, иначе это спецификация свойства
            int equalsPos = args[i].indexOf('=');
            if (equalsPos == -1) {
                try {
                    // Загружаем заданный класс
                    Class componentClass = Class.forName(args[i]);
                    // Создаем его экземпляр для создания экземпляра компонента
                    component = (Component) componentClass.newInstance();
                    BeanInfo componentBeanInfo = Introspector.getBeanInfo(componentClass);
                    properties = componentBeanInfo.getPropertyDescriptors();
                } catch (Exception ex) {
                    System.out.println("Невозвожно загрузить, создать экземпляh или провести интроспекцию: " + args[i]);
                    System.exit(1);
                }
                // Если всё прошло успешно, сохраняем компонент в векторе
                components.addElement(component);
            } else {
                String name = args[i].substring(0, equalsPos);
                String value = args[i].substring(equalsPos + 1);

                // Если у нас нет компонента, чтобы установить свойство, пропускаем
                if (component == null) continue nextarg;

                // Теперь просматриваем дескрипторы свойств этого компонента,
                // чтобы найти свойство с тем же именем.
                for (int j = 0; j < properties.length; j++) {
                    if (properties[j].getName().equals(name)) {
                        // Отлично, мы нашли свойство с нужным именем.
                        // Теперь получаем его тип и метод установки
                        Class type = properties[j].getPropertyType();
                        Method setter = properties[j].getWriteMethod();

                        // Проверяем, не является ли свойство доступным только для чтения
                        if (setter == null) {
                            System.err.println("Свойство " + name + " только для чтения");
                            continue nextarg;
                        }
                        // Пытаемся привести заданное значение свойства к правильному типу
                        // Здесь мы поддерживаем маленький набор обычных типов свойств
                        // Сохраняем преобразованное значение в Object[], так что
                        // теперь его будет удобно передать методу установки
                        try {
                            if (type == String.class) {
                                methodArgs[0] = value;
                            } else if (type == int.class) {
                                methodArgs[0] = Integer.valueOf(value);
                            } else if (type == boolean.class) {
                                methodArgs[0] = Boolean.valueOf(value);
                            } else if (type == Color.class) { // в Color (цвет)
                                methodArgs[0] = Color.decode(value);
                            } else if (type == Font.class) { // Строку в Font (шрифт)
                                methodArgs[0] = Font.decode(value);
                            } else {
                                // Если не можем преобразовать, пропускаем свойство
                                System.err.println("Свойство " + name + " неподдерживаемого типа " + type.getName());
                                continue nextarg;
                            }
                        } catch (Exception ex) {
                            // Если преобразование потерпело неудачу, переходим к следующему аргументу
                            System.err.println("Невозможно привести " + value + " к типу " + type.getName() + " для свойтсва " + name);
                            continue nextarg;
                        }
                        // Наконец, применяем отражение, чтобы вызвать метод
                        // установки для созданного нами компонента,
                        // и передаем ему преобразованное значение свойства.
                        try {
                            setter.invoke(component, methodArgs);
                        } catch (Exception ex) {
                            System.err.println("Невозможно установить свойтсво " + name);
                        }
                        // Теперь переходим к следующему заданному в командной строке аргументу
                        continue nextarg;
                    }
                }
                // Если мы оказались здесь, значит, не нашли заданное свойство
                System.err.println("Предупреждение: нет такого свойства: " + name);
            }
        }
        return components;
    }
}
