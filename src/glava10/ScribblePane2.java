package glava10;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Простой подкласс JPanel, который использует слушатели событий,
 * чтобы позволить пользователю рисовать при помощи мыши. Обратите внимание
 * на то, что рисунки не сохраняются и не перерисовываются.
 **/
public class ScribblePane2 extends JPanel {
    public ScribblePane2() {
        // Задаем желательный размер компонента
        setPreferredSize(new Dimension(450, 200));

        // Регистрируем обработчик событий мыши, определенный как внутренний класс
        // Обратите внимание на вызов requestFocus(). Это необходимо для того,
        // чтобы компонент принимал события клавиатуры.
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                moveto(e.getX(), e.getY()); // Перемещаемся в положение щелчка
                requestFocus(); //Запрашиваем фокус ввода
            }
        });

        // Регистрируем обработчик событий движения мыши, определенный
        // как внутренний класс посредством создания подкласса MouseMotionAdapter,
        // а не реализации MouseMotionListener. Мы замещаем только интересующие
        // нас методы и наследуем принимаемые по умолчанию (пустые)
        // реализации других методов.
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                lineto(e.getX(), e.getY()); // Проводим линию к положению мыши
            }
        });
        // Добавляем обработчик событий клавиатуры, чтобы по нажатию
        // клавиши 'С' стереть рисунок
        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_C) clear();
            }
        });
    }

    /* Это координаты предыдущего положения мыши */
    protected int last_x, last_y;

    /* Запоминаем заданную точку */
    public void moveto(int x, int y) {
        last_x = x;
        last_y = y;
    }

    /* Проводим линию от предыдущей точки к текущей,
     затем запоминаем новую точку */
    public void lineto(int x, int y) {
        Graphics g = getGraphics(); // Получаем объект, при помощи
        // которого будем рисовать
        g.setColor(color); // Сообщаем ему цвет, которым надо рисовать
        g.drawLine(last_x, last_y, x, y); // Сообщаем ему, что именно
        // следует рисовать
        moveto(x, y); // Сохраняем текущую точку
    }

    public void clear() {
        repaint();
    }

    // Это поле содержит текущее значение свойства color, т. е. цвета линии
    Color color = Color.black;

    // Это метод установки для свойства color

    public void setColor(Color color) {
        this.color = color;
    }

    // Это метод опроса для свойства color
    public Color getColor() {
        return color;
    }
}
