package glava16;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * Этот класс реализует удаленные методы, определенные
 * в интерфейсе RemoteBank. Хотя у него есть серьезный недостаток:
 * при выключении сервера все данные о счете будут потеряны
 **/
public class RemoteBankServer extends UnicastRemoteObject implements Bank.RemoteBank {
    // Этот вложенный класс хранит данные об отдельном банковском счете.
    class Account {
        String password;
        int balance;
        List transactions = new ArrayList();

        public Account(String password) {
            this.password = password;
            transactions.add("Счёт открыт " + new Date());
        }
    }

    // Эта хеш-таблица хранит все открытые счета и связывает
    // имя счета с объектом Account
    Map accounts = new HashMap();

    // Этот конструктор ничего не делает, но поскольку конструктор
    // базового класса генерирует исключение, это исключение
    // должно быть объявлено и здесь.
    public RemoteBankServer() throws RemoteException {
        super();
    }


    /* Открываем банковский счет с указанным именем и паролем
     * Этот метод сделан синхронизированным для обеспечения
     * потоковой безопасности, так как в нем происходит
     * манипулирование хештаблицей счетов.**/
    @Override
    public synchronized void openAccount(String name, String password) throws RemoteException, Bank.BankingException {
        // Проверяем, есть ли уже счет под таким именем
        if (accounts.get(name) != null) throw new Bank.BankingException("Счёт уже существует.");
        // Иначе он не существует, поэтому создаем
        Account acct = new Account(password);
        // и регистрируем его.
        accounts.put(name, acct);
    }

    /* Этот внутренний метод не является удаленным. Принимая имя и пароль,
     * он проверяет, не существует ли уже счет с таким именем и паролем.
     * Если это так, он возвращает объект Account.
     * Иначе – генерирует исключение.**/
    Account verify(String name, String password) throws Bank.BankingException {
        synchronized (accounts) {
            Account acct = (Account) accounts.get(name);
            if (acct == null) throw new Bank.BankingException("Нет такого счёта.");
            if (!password.equals(acct.password)) throw new Bank.BankingException("Неверный пароль.");
            return acct;
        }
    }

    /* Закрываем указанный счет. Этот метод сделан синхронизированным
     * для обеспечения потоковой безопасности, так как в нем
     * происходит манипулирование хештаблицей счетов */
    @Override
    public synchronized Bank.FunnyMoney closeAccount(String name, String password) throws RemoteException, Bank.BankingException {
        Account acct = verify(name, password);
        accounts.remove(name);
        // Перед изменением баланса или проведением проводок по любому счету
        // ради потоковой безопасности мы, прежде всего, должны получить
        // блокировку этого счета.
        synchronized (acct) {
            int balance = acct.balance;
            acct.balance = 0;
            return new Bank.FunnyMoney(balance);
        }
    }

    // Вносим заданную сумму FunnyMoney на указанный счет
    @Override
    public void deposit(String name, String password, Bank.FunnyMoney money) throws RemoteException, Bank.BankingException {
        Account acct = verify(name, password);
        synchronized (acct) {
            acct.balance += money.amount;
            acct.transactions.add("Внесено " + money.amount + " в " + new Date());
        }
    }

    // Снимаем заданную сумму с указанного счета
    @Override
    public Bank.FunnyMoney withdraw(String name, String password, int amount) throws RemoteException, Bank.BankingException {
        Account acct = verify(name, password);
        synchronized (acct) {
            if (acct.balance < amount) throw new Bank.BankingException("Недостаточно средств.");
            acct.balance -= amount;
            acct.transactions.add("Снято " + amount + " в " + new Date());
            return new Bank.FunnyMoney(amount);
        }
    }
    // Возвращаем текущий баланс для указанного счета
    @Override
    public int getBalance(String name, String password) throws RemoteException, Bank.BankingException {
        Account acct = verify(name, password);
        synchronized (acct) { return acct.balance; }
    }

    // Возвращаем вектор строк, содержащий историю проводок по указанному счету
    @Override
    public List getTransactionHistory(String name, String password) throws RemoteException, Bank.BankingException {
        Account acct = verify(name, password);
        synchronized (acct) {
            return acct.transactions;
        }
    }

    /*
     * Главная программа, выполняющая этот RemoteBankServer.
     * Создаем объект RemoteBankServer и присваиваем ему имя в реестре.
     * Считываем системное свойство для определения имени, а в качестве имени
     * по умолчанию используем имя FirstRemote. Это все, что необходимо
     * для настройки сервиса. Обо всем остальном позаботится RMI.
     **/
    public static void main(String[] args) {
        try {
            // Создаем объект банковского сервера
            RemoteBankServer bank = new RemoteBankServer();
            // Выясняем, как назвать его
            String name = System.getProperty("bankName", "FirstRemote");
            // Назовем его так
            Naming.rebind(name, bank);
            // Сообщаем миру, что мы готовы к работе
            System.out.println(name + " открыт и ждёт клиентов.");
        } catch (Exception e) {
            System.err.println(e);
            System.err.println("Формат: java [-Dbankname=<name>] com.davidflanagan.examples.rmi.RemoteBankServer");
            // Выходим принудительно из-за возможного существования потоков RMI.
            System.exit(1);
        }
    }
}
