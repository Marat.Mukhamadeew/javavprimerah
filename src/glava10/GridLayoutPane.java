package glava10;

import javax.swing.*;
import java.awt.*;

public class GridLayoutPane extends JPanel {
    public GridLayoutPane() {
        // Располагаем компоненты в решетке с тремя столбцами и числом строк,
        // зависящим от числа компонентов. Оставляем между компонентами
        // 10 пикселов по горизонтали и по вертикали
        this.setLayout(new GridLayout(0,3,10,10));

        // Помещаем компоненты в контейнер
        for (int i = 0; i <= 12; i++) {
            this.add(new JButton("Button #" + i));
        }
    }
}
