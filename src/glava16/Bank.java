package glava16;

import java.io.Serializable;
import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Данный класс является контейнером, просто содержащим
 * другие классы и интерфейсы для удаленной банковской системы
 **/

public class Bank {
    // Это интерфейс, в котором определены методы,
    // экспортируемые банковским сервером
    public interface RemoteBank extends Remote {
        // Открываем новый счет с указанным именем и паролем
        void openAccount(String name, String password) throws RemoteException, BankingException;

        // Закрываем именной счет
        FunnyMoney closeAccount(String name, String password) throws RemoteException, BankingException;

        // Вносим деньги на именной счет
        void deposit(String name, String password, FunnyMoney money) throws RemoteException, BankingException;

        // Снимаем заданную денежную сумму с именного счета
        FunnyMoney withdraw(String name, String password, int amount) throws RemoteException, BankingException;

        // Возвращаем сумму, хранящуюся на именном счете
        int getBalance(String name, String password) throws RemoteException, BankingException;

        // Возвращаем список строк, содержащий выписку проводок именного счета
        List<String> getTransactionHistory(String name, String password) throws RemoteException, BankingException;
    }

    /*
     * Этот простой класс представляет денежный счет. Данная реализация
     * представляет собой простую обертку вокруг целого числа.
     * Он полезен для демонстрации того, как RMI может принимать в качестве
     * аргументов произвольные нестроковые объекты и возвращать их
     * в качестве значений, если они являются сериализуемыми.
     * Более сложная реализация этого класса FunnyMoney может
     * включать серийный номер, цифровую подпись и другие средства
     * безопасности для обеспечения его уникальности и защиты.
     **/
    public static class FunnyMoney implements Serializable {
        public int amount;

        public FunnyMoney(int amount) {
            this.amount = amount;
        }
    }

    /*
     * Это тип исключения, используемого для представления
     * исключительных ситуаций, возникающих в банковском деле,
     * таких как “Недостаток денежных средств” и “Неверный пароль”
     **/
    public static class BankingException extends Exception {
        public BankingException(String msg) {
            super(msg);
        }
    }

    /*
     * Этот класс является простой автономной клиентской
     * программой, взаимодействующей с сервером RemoteBank.
     * Она вызывает различные методы RemoteBank в зависимости
     * от параметров ее командной строки и демонстрирует, как просто
     * при помощи RMI реализуется взаимодействие с сервером.
     **/
    public static class Client {
        public static void main(String[] args) {
            try {
                // Выясняем, с каким RemoteBank нужно соединяться,
                // путем чтения системного свойства (указанного
                // в командной строке с ключом –D) или, если оно
                // не задано, используем URL по умолчанию. Заметьте,
                // что по умолчанию клиент пытается соединиться
                // с сервером, расположенным на локальной машине
                String url = System.getProperty("bank", "rmi://FirstRemote");

                // Ищем этот сервер RemoteBank, используя объект
                // Naming, связанный с сервером rmiregistry.
                // По заданному url этот вызов возвращает объект
                // RemoteBank, методы которого могут быть вызваны удаленно
                RemoteBank bank = (RemoteBank) Naming.lookup(url);

                // Преобразуем команду пользователя к нижнему регистру
                String cmd = args[0].toLowerCase();

                // Проверяем команду на совпадение с возможными значениями
                if (cmd.equals("open")) {               // Открываем счет
                    bank.openAccount(args[1], args[2]);
                    System.out.println("Счёт открыт.");
                } else if (cmd.equals("close")) {       // Закрываем счет
                    FunnyMoney money = bank.closeAccount(args[1], args[2]);
                    // Наша валюта называется “деревянный никель”
                    System.out.println("Вам возвращено " + money.amount + " деревянных никелей.");
                    System.out.println("Большое спасибо.");
                } else if (cmd.equals("deposit")) {     // Внесение денег
                    FunnyMoney money = new FunnyMoney(Integer.parseInt(args[3]));
                    bank.deposit(args[1], args[2], money);
                    System.out.println("Внесено " + money.amount + " деревянных никелей.");
                } else if (cmd.equals("withdraw")) {    // Снятие денег
                    FunnyMoney money = bank.withdraw(args[1], args[2], Integer.parseInt(args[3]));
                    System.out.println("Снято " + money.amount + " деревянных никелей.");
                } else if (cmd.equals("balance")) {     // Проверка баланса счета
                    int amt = bank.getBalance(args[1], args[2]);
                    System.out.println("У вас в банке " + amt + " деревянных никелей.");
                } else if (cmd.equals("history")) {     // Получение выписки проводок
                    List<String> transactions = bank.getTransactionHistory(args[1], args[2]);
                    for (String transaction : transactions) {
                        System.out.println(transaction);
                    }
                } else System.out.println("Неизвестная команда");
                // Перехватываем и выводим исключения RMI
            } catch (RemoteException | BankingException ex) { System.err.println(ex); }
            // Перехватываем и выводим исключения, относящиеся к банковскому делу
            // Другие исключения – это, вероятно, синтаксические ошибки
            // пользователя, поэтому показываем строку формата.
            catch (Exception ex) {
                System.err.println(ex);
                System.err.println("Формат: java [-Dbank=<url>] Bank$Client " +
                        "<cmd> <name> <password> [<amount>]");
                System.err.println("где cmd это: open, close, deposit, "
                        + "withdraw, balance, history");
            }
        }
    }
}
